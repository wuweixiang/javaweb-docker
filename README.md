# javaweb-docker

#### 项目介绍
javaweb-docker旨在构建javaweb部署环境

#### 使用说明
1. docker exec -it  <容器ID>  bash   #进入容器
2. docker logs <容器ID>  #查看对应ID的日志
3. docker build -t  [<仓库名>[:<标签>]] .  #创建镜像

#### 使用方式
    1、  新购买的服务器，安装docker，执行以下指令：
     
     curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
     
     2、创建/var/webapps/目录，并将war包放入该目录下
     
     3、运行以下命令，即可实现部署。
     
     docker run -d -p 80:8080 \
     -v /var/webapps/：/var/apache-tomcat-8.5.35/webapps/ \ 
     --name <容器名>  \
     wuweixiang/javaweb:1.0.0
     
      
     
     注意： 
     
      该部署方式与之前的部署方式上，省去了jdk、tomcat环境的配置过程。
     
      挂载路径 /var/webapps/  为当前war上传位置
